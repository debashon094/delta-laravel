<?php
$asset = asset('/');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet" />

    <link href="https://unpkg.com/ionicons@4.5.10-0/dist/css/ionicons.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css"
        integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous" />

    <link rel="stylesheet" href="{{$asset}}plugins/mCustomScrollbar/jquery.mCustomScrollbar.min.css" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
    <link rel="stylesheet" href="{{$asset}}css/style.css" />
    <title>Greendelta</title>
</head>

<body class="dashboard aside-menu_unfold">
    <nav class="navbar navbar-light fixed-top">
        <div class="navbar-header">
            <a class="navbar-brand" href="index.html">
                <img src="{{$asset}}images/logo.png" alt="Greendelta" />
            </a>
            
        </div>
        <div class="navbar-container container-fluid">
            <div class="navbar-nav-content">
                <button class="btn btn-mobile_toggler">
                    <i class="ion ion-ios-menu"></i>
                </button>
                <div class="dropdown dropdown-user d-flex ml-auto">
                    <a href="#" class="dropdown-toggle" role="button" id="userNav" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img src="{{$asset}}images/avater.png" alt="" class="user-avater">
                        <span class="user-name">Md Kamal</span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userNav">
                        <a class="dropdown-item" href="#">Profile</a>
                        <a class="dropdown-item" href="#">Setting</a>
                        <a class="dropdown-item" href="#">Log out</a>
                    </div>
                </div>
            </div>
        </div>
    </nav>

    <aside class="aside-navbar">
        <div class="aside-navbar-body">
            <nav>
                <ul id="aside-nav" class="aside-nav">
                    <li class="nav-item active">
                        <a href="/dashboard" class="nav-link">
                            <i class="fas fa-tachometer-alt nav-icon"></i>
                            <span class="nav-title">Dashboard</span>
                        </a>
                    </li>
                    <li class="nav-item has-dropdown">
                        <a class="nav-link" data-toggle="collapse" href="#collapse__nav_1" role="button"
                            aria-expanded="false" aria-controls="collapse__nav_1">
                            <i class="fas fa-database nav-icon"></i>
                            <span class="nav-title">Master Data Setting</span>
                            <i class="ion ion-ios-arrow-forward nav-arrow"></i>
                        </a>
                        <div class="collapse" id="collapse__nav_1">
                            <ul class="nav flex-column sub-menu">
                                <li class="nav-item">
                                    <a href="/master-data-settings/banks" class="nav-link"><span class="nav-title">Bank Information's</span></a>
                                </li>
                                <li class="nav-item">
                                    <a href="/master-data-settings/employees" class="nav-link"><span class="nav-title">Employee
                                            Information's</span></a>
                                </li>
                                <li class="nav-item">
                                    <a href="fdr-renew.html" class="nav-link"><span class="nav-title">FDR Re-New</span></a>
                                </li>
                                <li class="nav-item">
                                    <a href="" class="nav-link"><span class="nav-title">Company Information's</span></a>
                                </li>
                                <li class="nav-item">
                                    <a href="" class="nav-link"><span class="nav-title">FDR Year Calculation</span></a>
                                </li>
                            </ul>
                        </div>
                    </li>

                    <li class="nav-item has-dropdown">
                        <a class="nav-link" data-toggle="collapse" href="#collapse__nav_2" role="button"
                            aria-expanded="false" aria-controls="collapse__nav_2">
                            <i class="fas fa-hand-holding-usd nav-icon"></i>
                            <span class="nav-title">FDR Transaction</span>
                            <i class="ion ion-ios-arrow-forward nav-arrow"></i>
                        </a>
                        <div class="collapse" id="collapse__nav_2">
                            <ul class="nav flex-column sub-menu">
                                <li class="nav-item">
                                    <a href="" class="nav-link"><span class="nav-title">FDR Posting</span></a>
                                </li>
                                <li class="nav-item">
                                    <a href="" class="nav-link"><span class="nav-title">FDR Transaction Form</span></a>
                                </li>
                            </ul>
                        </div>
                    </li>

                    <li class="nav-item has-dropdown">
                        <a class="nav-link" data-toggle="collapse" href="#collapse__nav_3" role="button"
                            aria-expanded="false" aria-controls="collapse__nav_3">
                            <i class="fas fa-flag-checkered nav-icon"></i>
                            <span class="nav-title">FDR Reporting</span>
                            <i class="ion ion-ios-arrow-forward nav-arrow"></i>
                        </a>
                        <div class="collapse" id="collapse__nav_3">
                            <ul class="nav flex-column sub-menu">
                                <li class="nav-item">
                                    <a href="bank-branch_&_period-wise.html" class="nav-link"><span class="nav-title">Bank Branch & Period Wise
                                            Report</span></a>
                                </li>
                                <li class="nav-item">
                                    <a href="date-wise-fdr-report-maturity.html" class="nav-link"><span class="nav-title">Date Wise FDR
                                            Report/Maturity</span></a>
                                </li>
                                <li class="nav-item">
                                    <a href="" class="nav-link"><span class="nav-title">Ledger Details Report</span></a>
                                </li>
                                <li class="nav-item">
                                    <a href="" class="nav-link"><span class="nav-title">FDR Average Rate Calcuation
                                            Report</span></a>
                                </li>
                                <li class="nav-item">
                                    <a href="" class="nav-link"><span class="nav-title">Term Basis FDR Report</span></a>
                                </li>
                                <li class="nav-item">
                                    <a href="" class="nav-link"><span class="nav-title">Bank List</span></a>
                                </li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </nav>
        </div>
    </aside>

    <main id="main-content">
        @yield('maincontent')
    </main>

    <footer class="footer"></footer>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="{{$asset}}plugins/jquery/jquery-3.2.1.js"></script>
    <script src="{{$asset}}plugins/popper/popper.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
    <script src="{{$asset}}plugins/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="{{$asset}}js/script.js"></script>

    @yield('additional_js')
</body>

</html>