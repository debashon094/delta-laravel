@extends('master.master')

@section('maincontent')
    <div class="content-container container-fluid">
	    <div class="page-heading">
	        <h1 class="heading-title">{{$title ?? ''}}</h1>
	    </div>

	    <div class="row">
	        <div class="col-md-4 col-lg-4 col-xl-2 mb-4">
	            <div class="card text-white text-center bg-blue card-statistics">
	                <div class="card-body">
	                    <h5 class="card-title text-white">700</h5>
	                    <p class="card-text">Total number of FDR</p>
	                </div>
	            </div>
	        </div>
	        <div class="col-md-4 col-lg-4 col-xl-2 mb-4">
	            <div class="card text-white text-center bg-purple card-statistics">
	                <div class="card-body">
	                    <h5 class="card-title text-white">500cr</h5>
	                    <p class="card-text">Total value of FDR</p>
	                </div>
	            </div>
	        </div>
	        <div class="col-md-4 col-lg-4 col-xl-2 mb-4">
	            <div class="card text-white text-center bg-success card-statistics">
	                <div class="card-body">
	                    <h5 class="card-title text-white">5cr</h5>
	                    <p class="card-text">Total Interest earned this month</p>
	                </div>
	            </div>
	        </div>
	        <div class="col-md-4 col-lg-4 col-xl-2 mb-4">
	            <div class="card text-white text-center bg-info card-statistics">
	                <div class="card-body">
	                    <h5 class="card-title text-white">20cr</h5>
	                    <p class="card-text">Total interest earned this quater</p>
	                </div>
	            </div>
	        </div>
	        <div class="col-md-4 col-lg-4 col-xl-2 mb-4">
	            <div class="card text-white text-center bg-warning card-statistics">
	                <div class="card-body">
	                    <h5 class="card-title text-white">150</h5>
	                    <p class="card-text">Total number of FDR renewals this month</p>
	                </div>
	            </div>
	        </div>
	        <div class="col-md-4 col-lg-4 col-xl-2 mb-4">
	            <div class="card text-white text-center bg-danger card-statistics">
	                <div class="card-body">
	                    <h5 class="card-title text-white">1cr</h5>
	                    <p class="card-text">Total excise duty this year</p>
	                </div>
	            </div>
	        </div>
	    </div>

	    <div class="card card-header-nobg-bb">
	        <div class="card-header">
	            <h6 class="mb-0">Current FDR</h6>
	        </div>
	        <div class="card-body">
	            <div class="table-responsive">
	                <table class="table table-border-rounded mb-0" style="min-width: 1200px">
	                    <thead>
	                        <tr>
	                            <th>#No</th>
	                            <th>Ref No.</th>
	                            <th>Bank</th>
	                            <th>Branch</th>
	                            <th>Interest Rate</th>
	                            <th>Total Interest</th>
	                            <th>Maturity Date</th>
	                            <th class="text-center">Status</th>
	                            <th class="text-center">Action</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                        <tr>
	                            <td>195</td>
	                            <td>4SBFV4PU</td>
	                            <td>Modhuntoti Bank Limited</td>
	                            <td class="text-ellipsis">Branch</td>
	                            <td>09.25%</td>
	                            <td>1,289,634</td>
	                            <td>07/00/0020</td>
	                            <td class="text-center">
	                                <div class="badge badge-success">Confrim</div>
	                            </td>
	                            <td class="text-center">
	                                <a href="" class="badge badge-secondary">
	                                    View/Edit
	                                </a>
	                            </td>
	                        </tr>
	                        <tr>
	                            <td>195</td>
	                            <td>4SBFV4PU</td>
	                            <td>Modhuntoti Bank Limited</td>
	                            <td class="text-ellipsis">Branch</td>
	                            <td>09.25%</td>
	                            <td>1,289,634</td>
	                            <td>07/00/0020</td>
	                            <td class="text-center">
	                                <div class="badge badge-success">Confrim</div>
	                            </td>
	                            <td class="text-center">
	                                <a href="" class="badge badge-secondary">
	                                    View/Edit
	                                </a>
	                            </td>
	                        </tr>
	                        <tr>
	                            <td>195</td>
	                            <td>4SBFV4PU</td>
	                            <td>Modhuntoti Bank Limited</td>
	                            <td class="text-ellipsis">Branch</td>
	                            <td>09.25%</td>
	                            <td>1,289,634</td>
	                            <td>07/00/0020</td>
	                            <td class="text-center">
	                                <div class="badge badge-success">Confrim</div>
	                            </td>
	                            <td class="text-center">
	                                <a href="" class="badge badge-secondary">
	                                    View/Edit
	                                </a>
	                            </td>
	                        </tr>
	                        <tr>
	                            <td>195</td>
	                            <td>4SBFV4PU</td>
	                            <td>Modhuntoti Bank Limited</td>
	                            <td class="text-ellipsis">Branch</td>
	                            <td>09.25%</td>
	                            <td>1,289,634</td>
	                            <td>07/00/0020</td>
	                            <td class="text-center">
	                                <div class="badge badge-success">Confrim</div>
	                            </td>
	                            <td class="text-center">
	                                <a href="" class="badge badge-secondary">
	                                    View/Edit
	                                </a>
	                            </td>
	                        </tr>
	                        <tr>
	                            <td>195</td>
	                            <td>4SBFV4PU</td>
	                            <td>Modhuntoti Bank Limited</td>
	                            <td class="text-ellipsis">Branch</td>
	                            <td>09.25%</td>
	                            <td>1,289,634</td>
	                            <td>07/00/0020</td>
	                            <td class="text-center">
	                                <div class="badge badge-success">Confrim</div>
	                            </td>
	                            <td class="text-center">
	                                <a href="" class="badge badge-secondary">
	                                    View/Edit
	                                </a>
	                            </td>
	                        </tr>
	                        <tr>
	                            <td>195</td>
	                            <td>4SBFV4PU</td>
	                            <td>Modhuntoti Bank Limited</td>
	                            <td class="text-ellipsis">Branch</td>
	                            <td>09.25%</td>
	                            <td>1,289,634</td>
	                            <td>07/00/0020</td>
	                            <td class="text-center">
	                                <div class="badge badge-success">Confrim</div>
	                            </td>
	                            <td class="text-center">
	                                <a href="" class="badge badge-secondary">
	                                    View/Edit
	                                </a>
	                            </td>
	                        </tr>
	                        <tr>
	                            <td>195</td>
	                            <td>4SBFV4PU</td>
	                            <td>Modhuntoti Bank Limited</td>
	                            <td class="text-ellipsis">Branch</td>
	                            <td>09.25%</td>
	                            <td>1,289,634</td>
	                            <td>07/00/0020</td>
	                            <td class="text-center">
	                                <div class="badge badge-success">Confrim</div>
	                            </td>
	                            <td class="text-center">
	                                <a href="" class="badge badge-secondary">
	                                    View/Edit
	                                </a>
	                            </td>
	                        </tr>
	                        <tr>
	                            <td>195</td>
	                            <td>4SBFV4PU</td>
	                            <td>Modhuntoti Bank Limited</td>
	                            <td class="text-ellipsis">Branch</td>
	                            <td>09.25%</td>
	                            <td>1,289,634</td>
	                            <td>07/00/0020</td>
	                            <td class="text-center">
	                                <div class="badge badge-success">Confrim</div>
	                            </td>
	                            <td class="text-center">
	                                <a href="" class="badge badge-secondary">
	                                    View/Edit
	                                </a>
	                            </td>
	                        </tr>
	                        <tr>
	                            <td>195</td>
	                            <td>4SBFV4PU</td>
	                            <td>Modhuntoti Bank Limited</td>
	                            <td class="text-ellipsis">Branch</td>
	                            <td>09.25%</td>
	                            <td>1,289,634</td>
	                            <td>07/00/0020</td>
	                            <td class="text-center">
	                                <div class="badge badge-success">Confrim</div>
	                            </td>
	                            <td class="text-center">
	                                <a href="" class="badge badge-secondary">
	                                    View/Edit
	                                </a>
	                            </td>
	                        </tr>
	                        <tr>
	                            <td>195</td>
	                            <td>4SBFV4PU</td>
	                            <td>Modhuntoti Bank Limited</td>
	                            <td class="text-ellipsis">Branch</td>
	                            <td>09.25%</td>
	                            <td>1,289,634</td>
	                            <td>07/00/0020</td>
	                            <td class="text-center">
	                                <div class="badge badge-success">Confrim</div>
	                            </td>
	                            <td class="text-center">
	                                <a href="" class="badge badge-secondary">
	                                    View/Edit
	                                </a>
	                            </td>
	                        </tr>
	                        <tr>
	                            <td>195</td>
	                            <td>4SBFV4PU</td>
	                            <td>Modhuntoti Bank Limited</td>
	                            <td class="text-ellipsis">Branch</td>
	                            <td>09.25%</td>
	                            <td>1,289,634</td>
	                            <td>07/00/0020</td>
	                            <td class="text-center">
	                                <div class="badge badge-success">Confrim</div>
	                            </td>
	                            <td class="text-center">
	                                <a href="" class="badge badge-secondary">
	                                    View/Edit
	                                </a>
	                            </td>
	                        </tr>
	                        
	                    </tbody>
	                </table>
	            </div>
	        </div>
	    </div>
	</div>
@stop