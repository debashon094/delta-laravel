@extends('master.master')

@section('maincontent')
	<div class="content-container container-fluid">
	    <div class="page-heading">
	        <h1 class="heading-title">{{$title ?? ''}}</h1>
	    </div>
	    <div class="card card-header-nobg-bb">
	        <div class="card-header">
	            <h6 class="mb-0">Bank</h6>
	        </div>
	        <div class="card-body">
	            <div class="d-flex align-items-center justify-content-between mb-2">
	                <div>
	                    <select name="" id="" class="custom-select mb-1">
	                        <option value="">All Bank</option>
	                        <option value="">Bank wise branch</option>
	                    </select>
	                </div>
	                <div>
	                    <input type="text" class="form-control mb-1" placeholder="Search by bank name...">
	                </div>
	            </div>
	            <div class="table-responsive">
	                <table class="table table-border-rounded mb-0" style="min-width: 1200px">
	                    <thead>
	                        <tr>
	                            <th>#ID</th>
	                            <th>Bank Name</th>
	                            <th>Branch</th>
	                            <th class="text-center">Action</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                        <tr>
	                            <td>195</td>
	                            <td>Modhuntoti Bank Limited</td>
	                            <td>Banani</td>
	                            <td class="text-center">
	                                <a href="" class="badge badge-secondary">View/Edit</a>
	                            </td>
	                        </tr>
	                        <tr>
	                            <td>196</td>
	                            <td>AB Bank Limited</td>
	                            <td>Banani</td>
	                            <td class="text-center">
	                                <a href="" class="badge badge-secondary">View/Edit</a>
	                            </td>
	                        </tr>
	                        <tr>
	                            <td>195</td>
	                            <td>Bangladesh Commerce Bank Limited</td>
	                            <td>Banani</td>
	                            <td class="text-center">
	                                <a href="" class="badge badge-secondary">View/Edit</a>
	                            </td>
	                        </tr>
	                        <tr>
	                            <td>197</td>
	                            <td>Bank Asia Limited</td>
	                            <td>Banani</td>
	                            <td class="text-center">
	                                <a href="" class="badge badge-secondary">View/Edit</a>
	                            </td>
	                        </tr>
	                        <tr>
	                            <td>198</td>
	                            <td>BRAC Bank Limited</td>
	                            <td>Banani</td>
	                            <td class="text-center">
	                                <a href="" class="badge badge-secondary">View/Edit</a>
	                            </td>
	                        </tr>
	                        <tr>
	                            <td>199</td>
	                            <td>City Bank Limited</td>
	                            <td>Banani</td>
	                            <td class="text-center">
	                                <a href="" class="badge badge-secondary">View/Edit</a>
	                            </td>
	                        </tr>
	                        <tr>
	                            <td>200</td>
	                            <td>Community Bank Bangladesh Limited</td>
	                            <td>Banani</td>
	                            <td class="text-center">
	                                <a href="" class="badge badge-secondary">View/Edit</a>
	                            </td>
	                        </tr>
	                        <tr>
	                            <td>201</td>
	                            <td>Dhaka Bank Limited</td>
	                            <td>Banani</td>
	                            <td class="text-center">
	                                <a href="" class="badge badge-secondary">View/Edit</a>
	                            </td>
	                        </tr>
	                        <tr>
	                            <td>202</td>
	                            <td>Dutch-Bangla Bank Limited</td>
	                            <td>Banani</td>
	                            <td class="text-center">
	                                <a href="" class="badge badge-secondary">View/Edit</a>
	                            </td>
	                        </tr>
	                        <tr>
	                            <td>203</td>
	                            <td>Eastern Bank Limited</td>
	                            <td>Banani</td>
	                            <td class="text-center">
	                                <a href="" class="badge badge-secondary">View/Edit</a>
	                            </td>
	                        </tr>
	                        <tr>
	                            <td>204</td>
	                            <td>IFIC Bank Limited</td>
	                            <td>Banani</td>
	                            <td class="text-center">
	                                <a href="" class="badge badge-secondary">View/Edit</a>
	                            </td>
	                        </tr>
	                        <tr>
	                            <td>205</td>
	                            <td>Jamuna Bank Limited</td>
	                            <td>Banani</td>
	                            <td class="text-center">
	                                <a href="" class="badge badge-secondary">View/Edit</a>
	                            </td>
	                        </tr>
	                        
	                    </tbody>
	                </table> 
	            </div>
	        </div>
	    </div> 
	</div>
@endsection