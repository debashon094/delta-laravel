<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@home');

Route::post('/login', 'AuthController@login');
Route::get('/logout', 'AuthController@logout');

Route::get('/dashboard', 'DashboardController@dashboard');


Route::group(['prefix' => 'master-data-settings'], function() {

	Route::resource('/banks', 'MasterData\BankController');
	Route::resource('/branches', 'MasterData\BranchController');
	Route::resource('/employees', 'MasterData\EmployeeController');
	Route::get('/fdrs', 'MasterData\FdrController@index');
	Route::get('/fdrs/renew', 'MasterData\FdrController@renew');
});

Route::group(['prefix' => 'fdr-fransaction'], function() {

	Route::get('/fdr-posting', 'FdrTransaction\TestController@test');
	Route::get('/fdr-transaction-form', 'FdrTransaction\TestController@test');
});

Route::group(['prefix' => 'fdr-reporting'], function() {

	Route::get('/bank-branch-and-period-wise-report', 'FdrReporting\ReportController@branchAndPeriodWiseReport');
	Route::get('/date-wise-fdr-report-maturity', 'FdrReporting\ReportController@dateWiseReportMaturity');
});