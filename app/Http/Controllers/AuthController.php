<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\UserLib\Login\LoginManager;
use Sentinel;

class AuthController extends Controller
{
    public function home()
    {
    	//return view('auth.login');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $email = $request->email;
        $password = $request->password;

        $login_manager = new LoginManager();
        $login_user = $login_manager->login( $email, $password );

        if( !$login_user ){
            dd("Sorry! could not login");
        }

        $res = $login_manager->complete( $request );

        dd('okk');
    }

    public function logout()
    {
        Sentinel::logout();

        dd("okk");
    }
}
